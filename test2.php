<!-- Add Film -->
<head>
<title>Films -- Add</title>
  <link rel="stylesheet" href="ver.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<ul class="nav nav-pills">
  <li> <a href="test.php">Film List</a>
  </li>
  <li class="active"><a href="test2.php">Add Film</a>
  </li>
</ul>

<h2>Add the film</h2>
<div class="col-xs-3">
<form method="POST" role="form-group">

    <input    class="form-control" id="ex2" type="text" name="year" value="" placeholder="Enter the year of the film" pattern="[0-9]{4}" required title="Разрешены только цифры"/><br/>
    <input    class="form-control" id="ex2" type="text" name="name" value="" placeholder="Enter the name of the movie"pattern="^[ 0-9]\s+${2,64}" required title="Разрешены только буквы "/><br/>
	 <a  href="test.php"><button class="btn btn-success">Add</button></a>
</div>

<?php
require_once "db.class.php";//include class DB
$db = new DB('mysql.hostinger.ru', 'u325251763_sg', '4561210', 'u325251763_sg');//create new object from class DB and provide path, user, pass and table name for database
if (isset ($_POST['deleteBook']) ){//if delete button has been pressed - delete book with this id
    $id = (int)$_POST['id'];
    $sql = "delete from films where id = {$id}";
    $db->query($sql);
}
if ( $_POST ){//update info about book
    $year = $db->escape($_POST['year']);
    $name = $db->escape($_POST['name']);
    if ( isset($_POST['id']) && (int)$_POST['id'] ){
        $id = (int)$_POST['id'];
        $sql = "UPDATE films
				SET year = '{$year}',
					name = '{$name}'
				WHERE id  = {$id}
				LIMIT 1
			";
        $db->query($sql);
    }else {//add information about NEW book
        $year = $db->escape($_POST['year']);
        $name = $db->escape($_POST['name']);
        $sql = "INSERT INTO films
				SET year = '{$year}',
					name = '{$name}'
			";
        $db->query($sql);
    }
	
}


?>
