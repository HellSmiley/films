<head>
<title>Films -- List</title>
  <link rel="stylesheet" href="ver.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>

<ul class="nav nav-pills">
  <li class="active">
    <a href="test.php">Film List</a>
  </li>
  <li><a href="test2.php">Add Film</a></li>
</ul>
      


<?php
require_once "db.class.php";//include class DB
$db = new DB('mysql.hostinger.ru', 'u325251763_sg', '4561210', 'u325251763_sg');//create new object from class DB and provide path, user, pass and table name for database
if (isset ($_POST['deleteBook']) ){//if delete button has been pressed - delete book with this id
    $id = (int)$_POST['id'];
    $sql = "delete from films where id = {$id}";
    $db->query($sql);
}
if ( $_POST ){//update info about book
    $year = $db->escape($_POST['year']);
    $name = $db->escape($_POST['name']);
    if ( isset($_POST['id']) && (int)$_POST['id'] ){
        $id = (int)$_POST['id'];
        $sql = "UPDATE films
				SET year = '{$year}',
					name = '{$name}'
				WHERE id  = {$id}
				LIMIT 1
			";
        $db->query($sql);
    }else {//add information about NEW book
        $year = $db->escape($_POST['year']); 
        $name = $db->escape($_POST['name']);
        $sql = "INSERT INTO films
				SET year = '{$year}',
					name = '{$name}'
			";
        $db->query($sql);
    }
}
//show list of the books (LIMIT 10 - how many)
$sql = "select * from films where isActive = 1 LIMIT 10";
$films = $db->query($sql);
//echo "<h2>The list of the films from DB:</h2>";
if ( !empty($films) ){
    foreach($films as $mem){
        ?>
		
        <form method="POST" role="form-group">
		<table class="table table-striped" style="width: 100%;">
		 <tr>
        <td style="width: 0.1%;"></td>
        <td style="width: 10%;">Year</td>
        <td style="width: 25%;">Name</td>
		<td style="width: 10%;"></td>
		<td style="width: 10%;"></td>
    </tr>
	
			<td>
            <input	type="hidden"  class="form-control" id="ex2"     name="id" value="<?=$mem['id']?>" /><br/>
			</td>
			<td>
            <input 	  class="form-control" id="ex2" type="text" name="year" value="<?=$mem['year']?>" pattern="[0-9]{4}" required title="Разрешены только цифры"/><br/>
			</td>
			<td>
            <input 	  class="form-control" id="ex2" type="text" name="name" value="<?=$mem['name']?>" pattern="^[ 0-9]\s+${2,64}" required title="Разрешены только буквы "/><br/>
			</td>
			<td>
            <input    class="btn btn-default" type="submit" value="Update" />
			</td>
			<td>
            <input    class="btn btn-default" type="submit" name="deleteBook" value="Delete" />
			</td>
			</table>
        </form>
		
        <?php

    }

}
?>
